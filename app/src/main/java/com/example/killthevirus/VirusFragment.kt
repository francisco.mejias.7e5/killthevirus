package com.example.killthevirus

import android.annotation.SuppressLint
import android.content.res.Resources
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Size
import android.view.*
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.killthevirus.databinding.FragmentVirusBinding
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.*
import kotlin.properties.Delegates

const val delay = 10L
const val baseSpeed = 8
const val variance = 4
const val virusLimit = 15
const val dieTime = 250L
const val arrowBlinkDelay = 1500L
const val arrowBlinkingDelay = 400L

class VirusFragment : Fragment() {
    private lateinit var binding: FragmentVirusBinding

    private var imageViewSize by Delegates.notNull<Int>()
    private lateinit var screenSize: Size
    private val variation get() = (-variance .. variance).random()
    private val imageViewAndVirus = mutableMapOf<ImageView, Virus>()
    private val virusQuantity get() = imageViewAndVirus.size
    private lateinit var arrows: List<ImageView>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentVirusBinding.inflate(inflater, container, false)

        screenSize = getScreenSize()
        imageViewSize = resources.getDimensionPixelSize(R.dimen.virus_wh)
        arrows = listOf(
            binding.arrowTop,
            binding.arrowLeft,
            binding.arrowTopLeft
        )

        tryBlinkArrow()

        binding.spreadButton.setOnVeryLongClickListener {
            Virus(it as ImageView, imageViewSize, screenSize, variation).start()
        }

        binding.root.setOnVeryLongClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                binding.cursedManGif.visibility = View.VISIBLE
                Glide.with(requireContext()).load(R.drawable.man_with_broom).into(binding.cursedManGif)
                delay(5000)
                imageViewAndVirus.forEach {
                    it.key.performClick()
                }
                binding.cursedManGif.setImageResource(R.drawable.thumbs_up_emoji)
                delay(1000)
                binding.cursedManGif.setImageResource(R.drawable.man_with_broom)
                binding.cursedManGif.visibility = View.INVISIBLE
            }
        }

        binding.spreadButton.setOnClickListener {
            if (virusQuantity < virusLimit) {
                createAndStartVirus()
            } else {
                Snackbar.make(binding.root, "Can't be more than $virusLimit virus at same time", Snackbar.LENGTH_SHORT).show()
            }
        }

        return binding.root
    }

    private fun tryBlinkArrow() {
        CoroutineScope(Dispatchers.Main).launch {
            delay(arrowBlinkDelay)
            arrows.forEach { it.visibility = View.VISIBLE }
            while (virusQuantity == 0) {
                arrows.map { async { it.animate().setDuration(arrowBlinkingDelay).alpha(0F).wait() } }.awaitAll()
                arrows.map { async { it.animate().setDuration(arrowBlinkingDelay).alpha(1F).wait() } }.awaitAll()
            }
            arrows.forEach { it.visibility = View.GONE; it.alpha = 0F }
        }
    }

    private fun createAndStartVirus() {
        val imageView = ImageView(requireContext(), null, 0, R.style.virus)
        imageView.layoutParams = ViewGroup.LayoutParams(imageViewSize, imageViewSize)
        imageView.translationX = (0 .. screenSize.width - imageViewSize).random().toFloat()
        imageView.translationY = (0 .. screenSize.height - imageViewSize).random().toFloat()
        binding.root.addView(imageView, virusQuantity)
        imageView.onDeathVirus()
        val virus = Virus(imageView, imageViewSize, screenSize, variation)
        virus.start()
        imageViewAndVirus[imageView] = virus
    }

    private fun ImageView.onDeathVirus() {
        this.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                imageViewAndVirus.remove(it)?.stop()
                this@onDeathVirus.setImageResource(R.drawable.virus_killed)
                delay(dieTime)
                binding.root.removeView(it)
                if (virusQuantity == 0) {
                    tryBlinkArrow()
                }
            }
        }
    }

    private fun getScreenSize(): Size {
        val metrics = Resources.getSystem().displayMetrics
        return Size(metrics.widthPixels, metrics.heightPixels)
    }

    // https://stackoverflow.com/a/53306100 with slightly modifications
    private fun View.setOnVeryLongClickListener(listener: (View) -> Unit) {
        setOnTouchListener(object : View.OnTouchListener {
            private val longClickDuration = 2000L
            private val handler = Handler(Looper.getMainLooper())

            @SuppressLint("ClickableViewAccessibility")
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                if (event?.action == MotionEvent.ACTION_DOWN) {
                    handler.postDelayed({ listener.invoke(this@setOnVeryLongClickListener) }, longClickDuration)
                } else if (event?.action == MotionEvent.ACTION_UP) {
                    this@setOnVeryLongClickListener.performClick()
                    handler.removeCallbacksAndMessages(null)
                }
                return true
            }
        })
    }

    private suspend fun ViewPropertyAnimator.wait() {
        delay(this@wait.duration)
    }
}
