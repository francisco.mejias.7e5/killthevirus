package com.example.killthevirus

import android.util.Size
import android.widget.ImageView
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class Virus(
    private val virus: ImageView,
    private val virusPixels: Int,
    private val screenSize: Size,
    variation: Int
) {
    private val speed = (-(baseSpeed + variation) .. (baseSpeed + variation)).step((baseSpeed + variation) * 2).toList()
    private var xSpeed = speed.random()
    private var ySpeed = speed.random()
    lateinit var mainJob: Job

    fun start() {
        mainJob = CoroutineScope(Dispatchers.Main).launch {
            while (this.isActive) {
                delay(delay)
                virus.translationX += xSpeed
                virus.translationY += ySpeed

                checkMarginsAndToggleSpeed()
            }
        }
    }

    private fun checkMarginsAndToggleSpeed() {
        if (virus.x >= screenSize.width - virusPixels || virus.x <= 0) {
            xSpeed *= -1
        }

        if (virus.y >= screenSize.height - virusPixels || virus.y <= 0) {
            ySpeed *= -1
        }
    }

    suspend fun stop() {
        mainJob.cancelAndJoin()
    }
}